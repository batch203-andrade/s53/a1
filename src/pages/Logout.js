import React, { useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import userContext from '../userContext';
import Swal from "sweetalert2";

export default function Logout() {
    const { unsetUser, setUser } = useContext(userContext);
    Swal.fire({
        title: "Logout Successful",
        icon: "success",
        text: "Comeback to Zuitt!"
    })
    unsetUser();
    useEffect(() => {
        return () => {
            setUser({ id: null, isAdmin: null });
        }
    }, [])
    return (
        <Navigate to="/login" />
    )
}
