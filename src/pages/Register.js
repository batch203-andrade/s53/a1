import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect } from 'react';
import { useContext } from 'react';
import userContext from '../userContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

function Register() {
    const { user } = useContext(userContext);
    const navigate = useNavigate();
    const [isActive, setIsActive] = useState(false);
    const [formData, setFormData] = useState({
        fName: "",
        lName: "",
        email: "",
        mobileNo: "",
        password1: "",
        password2: ""
    });

    function handleChange(event) {
        const { name, value } = event.target;
        setFormData(prevFormData => {
            return {
                ...prevFormData,
                [name]: value
            }
        });
    }

    useEffect(() => {
        if (
            formData.fName.length > 0 &&
            formData.lName.length > 0 &&
            formData.email.length > 0 &&
            formData.mobileNo.length > 0 &&
            formData.password1.length > 0 &&
            formData.password2.length > 0 &&
            formData.password1 === formData.password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [formData]);


    function registerUser(event) {
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: formData.email
                })
            }
        ).then(res => res.json())
            .then(data => {
                console.log(data)
                if (data) {
                    Swal.fire({
                        title: "Duplicate Email Found",
                        icon: "error",
                        text: "Kindly use another email."
                    })
                } else {
                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            firstName: formData.fName,
                            lastName: formData.lName,
                            email: formData.email,
                            password: formData.password1,
                            mobileNumber: formData.mobileNo
                        })
                    }).then(response => response.json())
                        .then(data => {
                            if (data) {
                                Swal.fire({
                                    title: "Registration Successful",
                                    icon: "success",
                                    text: "Welcome to Zuitt."
                                });

                                setFormData({
                                    fName: "",
                                    lName: "",
                                    email: "",
                                    mobileNo: "",
                                    password1: "",
                                    password2: ""
                                });

                                navigate("/login");
                                
                            } else {
                                Swal.fire({
                                    title: "Something went wrong",
                                    icon: "error",
                                    text: "Please try again"
                                });

                            }
                        })

                }
            });
    }
    return (
        // user.id != null
        //     ?
        //     <Navigate to="/courses" />
        //     :
        <>
            <h1 className='my-5 text-center'>Register</h1>
            <Form className="d-flex flex-column" onSubmit={registerUser}>
                <Form.Group className="mb-3" controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter First Name"
                        name="fName"
                        value={formData.fName}
                        onChange={handleChange}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your name with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Last Name"
                        name="lName"
                        value={formData.lName}
                        onChange={handleChange}
                        required

                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email"
                        placeholder="Enter email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                        required />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="mobileNumber">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control type="text" placeholder="+63<10 digit number> or 0<10 digit number>"
                        name="mobileNo"
                        value={formData.mobileNo}
                        onChange={handleChange}
                        required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter Password" name="password1"
                        value={formData.password1}
                        onChange={handleChange} required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Re-enter Password" name="password2"
                        value={formData.password2}
                        onChange={handleChange} required />
                </Form.Group>

                <Button variant={isActive ? "success" : "danger"} type="submit" id="submitBtn" className="col-2" disabled={!isActive}>
                    Login
                </Button>

            </Form>
        </>
    );
}

export default Register;