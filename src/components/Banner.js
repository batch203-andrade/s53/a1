import { Row, Col } from "react-bootstrap";
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner(prop) {


    return (
        <Row data-aos="fade-up">
            <Col className="p-5 text-center">
                <h1>{prop.title}</h1>
                <p>{prop.description}</p>
                <Button as={Link} to={prop.destination} variant="primary">{prop.label}</Button>
            </Col>
        </Row>
    )
}
