import React, { useEffect } from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import { useContext } from 'react';
import userContext from '../userContext';

export default function AppNavbar() {

    const { user } = useContext(userContext);

    return (
        <>
            <Navbar sticky="top" bg="light" expand="lg">
                <Container>
                    <Navbar.Brand as={NavLink} to="/">Zuitt</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ms-auto">
                            <Nav.Link as={NavLink} to="/" end>Home</Nav.Link>
                            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                            {
                                user.id != null
                                    ?
                                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                                    :
                                    <>
                                        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                                    </>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}
