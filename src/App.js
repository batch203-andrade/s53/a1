import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Error from "./pages/Error";
import { useEffect } from "react";
import AOS from 'aos';
import 'aos/dist/aos.css';
import { UserProvider } from "./userContext";
import { useState } from 'react';
import CourseView from "./pages/CourseView";


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  const authenticate = () => {
    localStorage.getItem("token") !== null
      ?
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      })
        .then(response => response.json())
        .then(data => {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        })
      :
      setUser({
        id: null,
        isAdmin: null
      })
  }

  useEffect(() => {
    authenticate();
  }, [])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <div className="App">
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/courses" element={<Courses />} />
              <Route exact path="/login" element={<Login />} />
              <Route exact path="/logout" element={<Logout />} />
              <Route exact path="/register" element={<Register />} />
              <Route exact path="/courses/:courseId" element={<CourseView />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </div>
    </UserProvider>
  );
}

export default App;
